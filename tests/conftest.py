#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import os
from flask import json
from flask.testing import FlaskClient
from mock import Mock
import pytest
from argus.main import create_app


class JsonClient(FlaskClient):
    def open(self, *args, **kwargs):
        json_data = kwargs.pop('json', None)
        if json_data:
            kwargs['data'] = json.dumps(json_data)
        headers = kwargs.setdefault('headers', {})
        headers['Content-Type'] = 'application/json'
        resp = super(JsonClient, self).open(*args, **kwargs)
        try:
            resp.json = json.loads(resp.data)
        except ValueError:
            pass
        return resp


@pytest.fixture
def app(request):
    logging.basicConfig(level=logging.DEBUG)
    os.environ['ARGUS_INFLUXDB_DATABASE'] = 'argus_test'
    a = create_app()
    a.config.debug = True
    a.config.testing = True
    a.test_client_class = JsonClient

    context = a.app_context()
    context.push()

    request.addfinalizer(context.pop)

    return a


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def tags():
    return [{
        'key': 'host',
        'value': 'mac',
    }, {
        'key': 'region',
        'value': 'sg',
    }]


@pytest.fixture
def timed_points():
    return [{
        'name': 'cpu',
        'value': 0.5,
        'timestamp': '2015-01-29T21:50:44Z'
    }, {
        'name': 'cpu',
        'value': 0.5,
        'timestamp': '2015-01-29T21:50:44Z'
    }]


@pytest.fixture
def points():
    return [{
        'name': 'cpu',
        'value': 0.5,
    }, {
        'name': 'cpu',
        'value': 0.5,
    }]


@pytest.fixture(autouse=True)
def influxdb_service(app, request, monkeypatch):
    if os.environ.get('CI_INFLUXDB', False):
        app.logger.debug('influxdb not available: mock it')
        mock = Mock()
        monkeypatch.setattr(
            'influxdb.influxdb08.client.InfluxDBClient.write_points', mock)
        return False
    else:
        app.logger.debug('influxdb available: clean it')
        app.influxdb.delete_database(app.config['INFLUXDB_DATABASE'])
        app.influxdb.create_database(app.config['INFLUXDB_DATABASE'])
        return True
